# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'c1e0b1d6aac0cc21e193e8552ce53b8f9a4d5181093119a69f73ede794e479c31ebdb140fef76ef64f5b00bc1ca991c3ced5ec7def4edb6acda8d130c0c78fa7'
